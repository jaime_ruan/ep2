import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Ajuda {
	
	private JFrame ajuda = new JFrame("Jogo da Cobrinha");
	private JLabel janela3 = new JLabel();
	private JButton voltar = new JButton("Voltar");
	private ImageIcon wallpaper;
	
	public Ajuda(){
		Voltar voltar2 = new Voltar();
		
		ajuda.setLayout(new BorderLayout());
		ajuda.setBounds(10, 10, 905, 700);
		ajuda.add(BorderLayout.CENTER, voltar);
		ajuda.setResizable(false);
		ajuda.setLocationRelativeTo(null);
		ajuda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		voltar.setBounds(300, 600, 300, 50);
		voltar.setFont(new Font("arial", Font.BOLD, 20));
	    voltar.addActionListener(voltar2);

	    wallpaper = new ImageIcon("wallpaper2.png");
		
		janela3.setIcon(wallpaper);
		ajuda.add(janela3);
		ajuda.setVisible(true);
	}
	
	static class Voltar implements ActionListener {
		public void actionPerformed(ActionEvent Voltar) {
			
			Menu menu = new Menu();
		}
	}
}
