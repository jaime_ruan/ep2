import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Gameplay3 extends JPanel implements KeyListener, ActionListener{
	
	private int[] snakexlength = new int[750];
	private int [] snakeylength = new int[750];
	
	private boolean left = false;
	private boolean right = false;
	private boolean up = false;
	private boolean down = false;

	private ImageIcon rightmouth2;
	private ImageIcon upmouth2;
	private ImageIcon downmouth2;
	private ImageIcon leftmouth2;
	
	private int lengthofsnake = 3;
	
	private Timer timer;
	private int delay = 100;
	private ImageIcon snakeimage2;
	
	private int[] barrierxlength = new int[750];
	private int[] barrierylength = new int[750];
	
	private ImageIcon barrierimage;
	
	private int over;
	
	private int [] fruitxpos = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,875,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int [] fruitypos = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	private int [] fruitnewxpos = {25,50,75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,875,525,550,575,600,625,650,675,700,725,750,775,800,825,850};
	private int [] fruitnewypos = {75,100,125,150,175,200,225,250,275,300,325,350,375,400,425,450,475,500,525,550,575,600,625};
	
	private int[] typefruit = {0, 1, 2};
	
	private ImageIcon simplefruitimage;
	private ImageIcon bombfruitimage;
	private ImageIcon bigfruitimage;
	private ImageIcon decreasefruitimage;
	
	private Random random = new Random();
	
	private int xpos = random.nextInt(34);
	private int ypos = random.nextInt(23);

	private int newxpos = random.nextInt(34);
	private int newypos = random.nextInt(23);

	private int fruits = random.nextInt(3);
	
	private int score = 0;
	
	private int moves = 0;
	
	private ImageIcon titleImage;
		
	public Gameplay3() {
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay, this);
		timer.start();
	}
	
	public void Barrier(Graphics g) {
		barrierxlength[9] = 500;
		barrierxlength[8] = 500;
		barrierxlength[7] = 500;
		barrierxlength[6] = 500;
		barrierxlength[5] = 500;
		barrierxlength[4] = 200;
		barrierxlength[3] = 200;
		barrierxlength[2] = 200;
		barrierxlength[1] = 200;
		barrierxlength[0] = 200;

		barrierylength[9] = 350;
		barrierylength[8] = 375;
		barrierylength[7] = 400;
		barrierylength[6] = 425;
		barrierylength[5] = 450;
		barrierylength[4] = 300;
		barrierylength[3] = 275;
		barrierylength[2] = 250;
		barrierylength[1] = 225;
		barrierylength[0] = 200;
	}
	
	public void paint(Graphics g) {
		
		Barrier(g);
		
		if(moves == 0) {
			snakexlength[2] = 50;
			snakexlength[1] = 75;
			snakexlength[0] = 100;
			
			snakeylength[2] = 100;
			snakeylength[1] = 100;
			snakeylength[0] = 100;
		}
		// cor da borda do titulo
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);
		
		// imagem do titulo
		titleImage = new ImageIcon("snaketitle.jpg");
		titleImage.paintIcon(this, g, 25, 11);
		
		// cor da borda do jogo
		g.setColor(Color.WHITE);
		g.drawRect(24, 74, 851, 577);
		
		// cor do interior do jogo
		g.setColor(Color.white);
		g.fillRect(25, 75, 850, 575);
		
		// cor do score
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Scores: "+score, 780, 30);
		
		// cor do "tamanho" da cobra
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Tamanho: "+(lengthofsnake-1), 780, 50);
		
		rightmouth2 = new ImageIcon("rightmouth2.png");
		rightmouth2.paintIcon(this, g, snakexlength[0], snakeylength[0]);
		
		for(int a=0; a<lengthofsnake; a++) {
			if(a==0 && right) {
				rightmouth2 = new ImageIcon("rightmouth2.png");
				rightmouth2.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			} 
			if(a==0 && left) {
				leftmouth2 = new ImageIcon("leftmouth2.png");
				leftmouth2.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			} 
			if(a==0 && down) {
				downmouth2 = new ImageIcon("downmouth2.png");
				downmouth2.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
			if(a==0 && up) {
				upmouth2 = new ImageIcon("upmouth2.png");
				upmouth2.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
			
			if(a!=0) {
				snakeimage2 = new ImageIcon("snakeimage2.png");
				snakeimage2.paintIcon(this, g, snakexlength[a], snakeylength[a]);
			}
		}
		
		barrierimage = new ImageIcon("barrier.jpeg");
		
		for (int i = 0; i < 10; i++) {
			barrierimage.paintIcon(this, g, barrierxlength[i], barrierylength[i]);
		}
		
		simplefruitimage = new ImageIcon("simplefruit.png");
		simplefruitimage.paintIcon(this, g, fruitxpos[xpos], fruitypos[ypos]);
		
		if((fruitxpos[xpos] == snakexlength[0] && fruitypos[ypos] == snakeylength[0])) {
			score++;
			lengthofsnake++;
			
			xpos = random.nextInt(34);
			ypos = random.nextInt(23);

			newxpos = random.nextInt(34);
			newypos = random.nextInt(23);
			
			fruits = random.nextInt(3);
		}
		
		if(newxpos == xpos && newypos == ypos) {

            newxpos = random.nextInt(34);
            newypos = random.nextInt(23);
        }
		
		if(score % 5 == 0 && score > 0) {

            if(typefruit[fruits] == 0) {

                bombfruitimage = new ImageIcon("bombfruit.png");
                bombfruitimage.paintIcon(this, g, fruitnewxpos[newxpos], fruitnewypos[newypos]);

                if((fruitnewxpos[newxpos] == snakexlength[0] && fruitnewypos[newypos] == snakeylength[0])) {
                    right = false;
                    left = false;
                    up = false;
                    down = false;

                    g.setColor(Color.black);
    				g.setFont(new Font("arial", Font.BOLD, 50));
    				g.drawString("Game Over", 300, 300);
    				
    				g.setFont(new Font("arial", Font.BOLD, 20));
    				g.drawString("Space to RESTART", 350, 340);
    				
    				g.setFont(new Font("arial", Font.BOLD, 20));
    				g.drawString("Esc to RETURN", 370, 380);
    				
    				g.setFont(new Font("arial", Font.BOLD, 20));
    				g.drawString("Or use ↔↕ to continue", 330, 420);
                }
            }

            if(typefruit[fruits] == 1) {
                bigfruitimage = new ImageIcon("bigfruit.png");
                bigfruitimage.paintIcon(this, g, fruitnewxpos[newxpos], fruitnewypos[newypos]);

                if((fruitnewxpos[newxpos] == snakexlength[0] && fruitnewypos[newypos] == snakeylength[0])) {
                	newxpos = random.nextInt(34);
                	newypos = random.nextInt(23);

                    fruits = random.nextInt(3);

                    score++;
                    score++;

                    lengthofsnake++;
                }
            }

            if(typefruit[fruits] == 2) {

                decreasefruitimage = new ImageIcon("decreasefruit.png");
                decreasefruitimage.paintIcon(this, g, fruitnewxpos[newxpos], fruitnewypos[newypos]);

                if((fruitnewxpos[newxpos] == snakexlength[0] && fruitnewypos[newypos] == snakeylength[0])) {

                    newxpos = random.nextInt(34);
                    newypos = random.nextInt(23);

                    fruits = random.nextInt(3);

                    lengthofsnake = 3;
                }
            }
        }

		for(int b=1 ; b<lengthofsnake ; b++) {
			if((snakexlength[b] == snakexlength[0]) && (snakeylength[b] == snakeylength[0])) {
				right = false;
				left = false;
				up = false;
				down = false;
				
				g.setColor(Color.black);
				g.setFont(new Font("arial", Font.BOLD, 50));
				g.drawString("Game Over", 300, 300);
				
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350, 340);
				
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Esc to RETURN", 370, 380);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Or use ↔↕ to continue", 330, 420);
			}
		}
		
		for (int j=0 ; j<10 ; j++) {
			if (snakexlength[0] == barrierxlength[j] && snakeylength[0] == barrierylength[j]) {
				right = false;
				left = false;
				up = false;
				down = false;
				
				g.setColor(Color.black);
				g.setFont(new Font("arial", Font.BOLD, 50));
				g.drawString("Game Over", 300, 300);
				
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Space to RESTART", 350, 340);
				
				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Esc to RETURN", 370, 380);

				g.setFont(new Font("arial", Font.BOLD, 20));
				g.drawString("Or use ↔↕ to continue", 330, 420);
			}
		}
			
		g.dispose();
	}

	public void actionPerformed(ActionEvent e) {
		timer.start();
		if(right) {
			for(int r = lengthofsnake-1; r>=0 ; r--) {
				snakeylength[r+1] = snakeylength[r];
			}
			for(int r = lengthofsnake; r>=0 ; r--) {
				if(r==0) {
					snakexlength[r] = snakexlength[r] + 25;
				}else {
					snakexlength[r] = snakexlength[r-1];
				}
				if(snakexlength[r] > 850) {
					snakexlength[r] = 25;
				}
			}
			repaint();
		}
		if(left) {
			for(int r = lengthofsnake-1; r>=0 ; r--) {
				snakeylength[r+1] = snakeylength[r];
			}
			for(int r = lengthofsnake; r>=0 ; r--) {
				if(r==0) {
					snakexlength[r] = snakexlength[r] - 25;
				}else {
					snakexlength[r] = snakexlength[r-1];
				}
				if(snakexlength[r] < 25) {
					snakexlength[r] = 850;
				}
			}
			repaint();
		}
		if(up) {
			for(int r = lengthofsnake-1; r>=0 ; r--) {
				snakexlength[r+1] = snakexlength[r];
			}
			for(int r = lengthofsnake; r>=0 ; r--) {
				if(r==0) {
					snakeylength[r] = snakeylength[r] - 25;
				}else {
					snakeylength[r] = snakeylength[r-1];
				}
				if(snakeylength[r] < 75) {
					snakeylength[r] = 625;
				}
			}
			repaint();
		}
		if(down) {
			for(int r = lengthofsnake-1; r>=0 ; r--) {
				snakexlength[r+1] = snakexlength[r];
			}
			for(int r = lengthofsnake; r>=0 ; r--) {
				if(r==0) {
					snakeylength[r] = snakeylength[r] + 25;
				}else {
					snakeylength[r] = snakeylength[r-1];
				}
				if(snakeylength[r] > 625) {
					snakeylength[r] = 75;
				}
			}
			repaint();
		}
	}
	
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
			Menu2 menu2 = new Menu2();
		}
		if(e.getKeyCode() == KeyEvent.VK_SPACE) {
			moves = 0;
			score = 0;
			lengthofsnake = 3;
			repaint();
		}
		if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
			moves++;
			right = true;
			
			if(!left) {
				right = true;
			} else {
				right = false;
				left = true;
			}

			up = false;
			down = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT) {
			moves++;
			left = true;
			
			if(!right) {
				left = true;
			} else {
				left = false;
				right = true;
			}

			up = false;
			down = false;
		}		
		if(e.getKeyCode() == KeyEvent.VK_UP) {
			moves++;
			up = true;
			
			if(!down) {
				up = true;
			} else {
				down = false;
				right = true;
			}

			left = false;
			right = false;
		}
		if(e.getKeyCode() == KeyEvent.VK_DOWN) {
			moves++;
			down = true;
			
			if(!up) {
				down = true;
			} else {
				up = false;
				down = true;
			}

			left = false;
			right = false;
		}
		if (moves == 0) {
			if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_LEFT) {
				moves++;
				right = true;
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}