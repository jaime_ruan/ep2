import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Menu{
	
	private JFrame menu = new JFrame("Jogo da Cobrinha");
	private JLabel janela = new JLabel();
	private JButton modo = new JButton("Modos de Jogo");
	private JButton ajuda = new JButton("Ajuda");
	private JButton sair = new JButton("Sair");
	private ImageIcon wallpaper;
	
	public Menu(){

	    Modo play = new Modo();
		Ajuda2 help = new Ajuda2();
		Sair end  = new Sair();
	
		menu.setLayout(new BorderLayout());
		menu.setBounds(10, 10, 905, 700);
		menu.add(BorderLayout.CENTER, modo);
		menu.add(BorderLayout.CENTER, ajuda);
		menu.add(BorderLayout.CENTER, sair);
		menu.setResizable(false);
		menu.setLocationRelativeTo(null);
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		modo.setBounds(300, 240, 300, 50);
		modo.setFont(new Font("arial", Font.BOLD, 20));
	    modo.addActionListener(play);
		
		ajuda.setBounds(300, 300, 300, 50);
		ajuda.setFont(new Font("arial", Font.BOLD, 20));
		ajuda.addActionListener(help);
		
		sair.setBounds(300, 360, 300, 50);
		sair.setFont(new Font("arial", Font.BOLD, 20));
		sair.addActionListener(end);
		
		wallpaper = new ImageIcon("wallpaper.jpg");
		
		janela.setIcon(wallpaper);
		menu.add(janela);
		menu.setVisible(true);
		
	}
	
	static class Modo implements ActionListener {
		public void actionPerformed(ActionEvent Modo) {
			
			Menu2 menu2 = new Menu2();
		}
	}
	
	static class Ajuda2 implements ActionListener {
		public void actionPerformed(ActionEvent Ajuda2) {
			
			Ajuda ajuda = new Ajuda();
		}
	}
	
	static class Sair implements ActionListener {
		public void actionPerformed(ActionEvent Sair) {
			
			System.exit(0);
		}
	}
}