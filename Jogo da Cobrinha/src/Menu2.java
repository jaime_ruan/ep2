import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Menu2{
	
	private JFrame menu2 = new JFrame("Jogo da Cobrinha");
	private JLabel janela2 = new JLabel();
	private JButton comum = new JButton("Comum");
	private JButton kitty = new JButton("Kitty");
	private JButton star = new JButton("Star");
	private JButton sair2 = new JButton("Voltar");
	private ImageIcon wallpaper;
	
	public Menu2(){
		
	    Comum comum2 = new Comum();
		Kitty kitty2 = new Kitty();
		Star star2  = new Star();
		Sair2 end2  = new Sair2();
	
		menu2.setLayout(new BorderLayout());
		menu2.setBounds(10, 10, 905, 700);
		menu2.add(BorderLayout.CENTER, comum);
		menu2.add(BorderLayout.CENTER, kitty);
		menu2.add(BorderLayout.CENTER, star);
		menu2.add(BorderLayout.CENTER, sair2);
		menu2.setResizable(false);
		menu2.setLocationRelativeTo(null);
		menu2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		comum.setBounds(300, 240, 300, 50);
		comum.setFont(new Font("arial", Font.BOLD, 20));
	    comum.addActionListener(comum2);
		
		kitty.setBounds(300, 300, 300, 50);
		kitty.setFont(new Font("arial", Font.BOLD, 20));
		kitty.addActionListener(kitty2);
		
		star.setBounds(300, 360, 300, 50);
		star.setFont(new Font("arial", Font.BOLD, 20));
		star.addActionListener(star2);
		
		sair2.setBounds(300, 420, 300, 50);
		sair2.setFont(new Font("arial", Font.BOLD, 20));
		sair2.addActionListener(end2);
		
		wallpaper = new ImageIcon("wallpaper.jpg");
		
		janela2.setIcon(wallpaper);
		menu2.add(janela2);
		menu2.setVisible(true);
	}
	
	static class Comum implements ActionListener {
		public void actionPerformed(ActionEvent Comum) {
			
			JFrame obj = new JFrame();
			Gameplay gameplay = new Gameplay();
			
			obj.setBounds(10, 10, 905, 700);
			obj.setBackground(Color.lightGray);
			obj.setResizable(false);
			obj.setVisible(true);
			obj.setLocationRelativeTo(null);
			obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			obj.add(gameplay);
		}
	}
	
	static class Kitty implements ActionListener {
		public void actionPerformed(ActionEvent Kitty) {
			
			JFrame obj = new JFrame();
			Gameplay2 gameplay2 = new Gameplay2();
			
			obj.setBounds(10, 10, 905, 700);
			obj.setBackground(Color.lightGray);
			obj.setResizable(false);
			obj.setVisible(true);
			obj.setLocationRelativeTo(null);
			obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			obj.add(gameplay2);
		}
	}
	
	static class Star implements ActionListener {
		public void actionPerformed(ActionEvent Star) {

			JFrame obj = new JFrame();
			Gameplay3 gameplay3 = new Gameplay3();
			
			obj.setBounds(10, 10, 905, 700);
			obj.setBackground(Color.lightGray);
			obj.setResizable(false);
			obj.setVisible(true);
			obj.setLocationRelativeTo(null);
			obj.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			obj.add(gameplay3);
		}
	}
	
	static class Sair2 implements ActionListener {
		public void actionPerformed(ActionEvent Sair2) {
			
			Menu menu = new Menu();
		}
	}
}