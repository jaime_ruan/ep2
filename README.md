

# EP2 - OO 2019.2 (UnB - Gama)

    • Turma: A - Renato
    • Aluno: Jaime Juan de Castro Feliciano Damasceno
    • Data de entrega: 12/11/2019

## Descrição:

Por meio das setas do teclado o jogador controla uma cobra que se arrasta pela tela, coletando variadas frutas (cada uma com sua característica), por uma determinada área (variável de acordo com o modo de jogo). Cada vez que a serpete se alimenta de determinada fruta o jogo implementa uma habilidade especial, aumentando a dificuldade do jogo. Se perder, apertar ESPAÇO para recomeçar, ESC para voltar ao menu anterior ou as setinhas para continuar.

## Modos de Jogo:

    • Comum:  A Snake classica, sem habilidades especiais.
    • Kitty: Essa Snake tem as habilidades de atravessar as barreiras do jogo, mas não pode atravessar as bordas nem a si mesma.
    • Star: Recebe o dobro de pontos ao comer as frutas, não atravessa as barreiras do jogo, mas pode atravessar as bordas.
    • Obs: Um dos critérios para ponto extra implementado no modo "Star" (atravessar as bordas).

## Frutas:

    • Simple Fruit: Fruta comum, dá um ponto e aumenta o tamanho da cobra.
    • Bomb Fruit: Essa fruta deve levar a morte da Snake.
    • Big Fruit: Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
    • Decrease Fruit: Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.

## Score:

Os pontos são acrecidos com o alimentar da cobra para determinada fruta.

## Dependências

Jogo realizado no Eclipse IDE 2019.09. Versão Java - 11.

    • import java.awt.Color;
    • import java.awt.Font;
    • import java.awt.Graphics;
    • import java.awt.event.ActionEvent;
    • import java.awt.event.ActionListener;
    • import java.awt.event.KeyEvent;
    • import java.awt.event.KeyListener;

    • import java.util.Random;

    • import javax.swing.ImageIcon;
    • import javax.swing.JOptionPane;
    • import javax.swing.JPanel;
    • import javax.swing.Timer;

## Rodar o Jogo

Deve-se ter uma JVM (Java Virtual Machine) instalada e uma IDE, de preferência o Eclipse, e após isso, rodar o código no botão "Run" para assim abrir a janela do jogo.
Obs: Ficar atento, pois pode ser que as imagens não sejam carregadas. Dê preferência ao Eclipse.

## Diagrama de Classe

![](Jogo da Cobrinha/Diagram.png)
Obs: Não foi possível anexar a foto em questão, porém a mesma encontra-se na pasta do projeto.
